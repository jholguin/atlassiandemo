'use strict';

module.exports = function (grunt) {

    var base = 'src/';
    var dist = 'app/';
    var npm = 'node_modules/'

    // Load grunt tasks automatically
    require('load-grunt-config')(grunt, {
        data: {
            paths: {
                src: base,
                dist: dist,
                node_modules: npm
            }  
        }
    });
};