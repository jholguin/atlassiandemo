"use strict";

var app = angular.module("Atlassian", []);
//Global variable to keep json obj in one value so we don't need to keep calling get to save on js calls;
var atObj = [];

app.controller('TabController',[ "$scope", "$http", "$location", "$anchorScroll", function($scope, $http, $location,$anchorScroll){

	
	$scope.tracks = [];
	$scope.sessions = [];
	$scope.sessionDescription;
	$scope.sessionSection;
	
	//Inital call to json obj to get data for furture use
	$http.get("sessions.json").then(function(response){
		atObj = response.data.Items;
		
		angular.forEach(response.data.Items, function(value, key){
			
			if($scope.tracks.indexOf(value.Track.Title) == -1){
				$scope.tracks.push(value.Track.Title);
			}
		});

		$scope.sessions = getSessionsByTrack(atObj, $scope.tracks[0]);

		

		//Initial first Session to show
		$scope.sessionDescription = $scope.sessions[0];
		$scope.sessionSection = $scope.sessions[0].Track.Title;
	});

	//Filter Json object to find session id and update the descritption with selected session id
	$scope.showSession = function(id){
      	$anchorScroll('description');
		
		$scope.sessionDescription = atObj.filter(function(item){
     		return item.Id==id; 
    	})[0];
	}

	//Main track function, will search for tracks and list data based on selected track
	$scope.showTrack = function(track){

		$scope.sessions = getSessionsByTrack(atObj, track);
		$scope.sessionDescription = $scope.sessions[0];
		$scope.sessionSection = track;
	}


	//Helper function to get all session based on a certain track, accepts the array of values and a key(track) to search for
	//<param item="obj">sessObj</param>
	//<param item="string">track</param>
	//<return>Array of objects</return>
	function getSessionsByTrack(sessObj, track){
		var sessionObj = [];
		
		//Build Session list
		angular.forEach(sessObj, function(value, key){
			if(value.Track.Title == track){
				sessionObj.push(value);
			}
		});

		return sessionObj;
	}

}]);


