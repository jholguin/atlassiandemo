# Atlassian Demo #

This is the repo for my Atlassian demo project given to me by them

## Requirements

You'll need to have the following items installed before continuing.

  * [Node.js](http://nodejs.org): Use the installer provided on the NodeJS website.
  * [Grunt](http://gruntjs.com/): Run `[sudo] npm install -g grunt-cli`
  
  
## Getting Started

  1. Install Dependencies
  
  		npm install
  		
  2. Build the client-side libraries
  		
  		grunt
		
## Folder Structure
  * app - contains all compiled source files
  * grunt - contains all grunt task 
  * node_modules - (will appear once npm has been run) contains all dependencies 
  * src - contains all source materials, where all work will be done

		
You are all done!!! Once the grunt file is run it will compile all the code into the app folder. If you choose not to build the project, there will already be a compiled source for you that you can point your sever to and view or drab to a browswer of your choice. 