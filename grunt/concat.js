module.exports = {
    dist:{
        src: [
            '<%= paths.node_modules %>angular/angular.js',
            '<%= paths.src %>lib/scripts/app.js'
        ],
        dest: '<%= paths.dist %>scripts/app.js'
    }
}