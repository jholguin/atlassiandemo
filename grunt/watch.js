module.exports = {
	options: {
    	livereload: false
    },  
    scripts: {
        files: [ '<%= paths.src %>/lib/scripts/*.js'],
        tasks: ['concat']
    },
    sass: {
    	files: [ '<%= paths.src %>/lib/styles/**/*.scss' ],
        tasks: ['build-css']
    },
    html:{
        files: ['<%= paths.src %>*.html'],
        task: ['build']
    },
    images:{
        files: ['<%= paths.src %>lib/*'],
        task: ['copy:images']
    }
}