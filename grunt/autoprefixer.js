module.exports = {
    dev: {
        expand: true,
        flatten: true,
        src: '<%= paths.dist %>/styles/*.css',
        dest: '<%= paths.dist %>/styles'
    }
};
