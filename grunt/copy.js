module.exports = {
    html: {
       files: [
            {
                expand: true,
                cwd: '<%= paths.src %>',
                src: ['*.html'],
                dest: '<%= paths.dist %>'
            }
        ]
    },
    images: {
        files: [
            {
                expand: true,
                cwd: '<%= paths.src %>lib/images',
                src: '*',
                dest: '<%= paths.dist %>images'
            }
        ]
    },
    json: {
       files: [
            {
                expand: true,
                cwd: '<%= paths.src %>lib/',
                src: '*.json',
                dest: '<%= paths.dist %>'
            }
        ] 
    }
}