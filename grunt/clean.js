module.exports = {
	build: {
		src: '<%= paths.dist %>'	
	},
	html: {
		src: '<%= paths.dist %>*.html'
	}
};